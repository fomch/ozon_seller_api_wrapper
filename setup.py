from setuptools import setup, find_packages

setup(
    name="ozon_seller_api_wrapper",
    version="0.4",
    packages=find_packages(),
    description="Ozon Seller API wrapper",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Fomchenkov Vyacheslav",
    author_email="fomchenkov.dev@gmail.com",
    url="https://gitlab.com/fomch/ozon_seller_api_wrapper",
    install_requires=[
        "requests",
        "deprecated",
    ],
)
