# Обертка для работы с официальным Ozon Seller API на Python

[https://docs.ozon.ru/api/seller/](https://docs.ozon.ru/api/seller/)

## Установка

```
pip install -U git+https://gitlab.com/fomch/ozon_seller_api_wrapper
```

## Пример использования

```python
from ozon_seller_api_wrapper import PricesandStocksAPI

price_stocks_api = PricesandStocksAPI("client_id", "client_secret")
print(price_stocks_api.v3_product_info_stocks())
```

## Поддерживаемые разделы API

- [x] Атрибуты и характеристики Ozon
- [ ] Загрузка и обновление товаров
- [x] Штрихкоды товаров
- [x] Цены и остатки товаров
- [ ] Акции
- [ ] Стратегии ценообразования
- [x] Сертификаты брендов
- [ ] Сертификаты качества
- [x] Склады
- [ ] Обработка заказов FBS и rFBS
- [x] Полигоны
- [ ] Доставка FBO
- [ ] Управление кодами маркировки и сборкой заказов для FBS/rFBS
- [ ] Доставка FBS
- [ ] Доставка rFBS
- [ ] Пропуски
- [x] Возвраты товаров FBO и FBS
- [ ] Возвраты товаров rFBS
- [ ] Возвратные отгрузки
- [ ] Отмены заказов
- [ ] Чаты с покупателями
- [ ] Накладные
- [ ] Отчёты
- [x] Аналитические отчёты
- [x] Финансовые отчёты
- [x] Рейтинг продавца
