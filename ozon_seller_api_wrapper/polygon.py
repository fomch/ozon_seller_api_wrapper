import requests


class PolygonAPI:
    """
    Полигоны

    https://docs.ozon.ru/api/seller/#tag/PolygonAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v1_polygon_create(self, coordinates):
        """
        Создайте полигон доставки

        https://docs.ozon.ru/api/seller/#operation/PolygonAPI_CreatePolygon
        """

        data = {
            "coordinates": coordinates,
        }
        url = "https://api-seller.ozon.ru/v1/polygon/create"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_polygon_bind(self, delivery_method_id, polygons, warehouse_location):
        """
        Свяжите метод доставки с полигоном доставки

        https://docs.ozon.ru/api/seller/#operation/PolygonAPI_BindPolygon
        """

        data = {
            "delivery_method_id": delivery_method_id,
            "polygons": polygons,
            "warehouse_location": warehouse_location,
        }
        url = "https://api-seller.ozon.ru/v1/polygon/bind"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
