import requests


class SellerRating:
    """
    Рейтинг продавца

    https://docs.ozon.ru/api/seller/#tag/SellerRating
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v1_rating_summary(self):
        """
        Получить информацию о текущих рейтингах продавца

        https://docs.ozon.ru/api/seller/#operation/RatingAPI_RatingSummaryV1
        """

        data = {}
        url = "https://api-seller.ozon.ru/v1/rating/summary"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_rating_history(self, date_from, date_to, ratings, with_premium_scores):
        """
        Получить информацию о рейтингах продавца за период

        https://docs.ozon.ru/api/seller/#operation/RatingAPI_RatingHistoryV1
        """

        data = {
            "date_from": date_from,
            "date_to": date_to,
            "ratings": ratings,
            "with_premium_scores": with_premium_scores,
        }
        url = "https://api-seller.ozon.ru/v1/rating/history"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
