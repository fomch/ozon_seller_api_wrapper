import requests


class PricesandStocksAPI:
    """
    Цены и остатки товаров

    https://docs.ozon.ru/api/seller/#tag/PricesandStocksAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v2_products_stocks(self, stocks):
        """
        Обновить количество товаров на складах

        https://docs.ozon.ru/api/seller/#operation/ProductAPI_ProductsStocksV2
        """

        data = {
            "stocks": stocks,
        }
        url = "https://api-seller.ozon.ru/v2/products/stocks"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v3_product_info_stocks(self, filter={}, last_id=None, limit=100):
        """
        Информация о количестве товаров

        https://docs.ozon.ru/api/seller/#operation/ProductAPI_GetProductInfoStocksV3
        """

        data = {
			"filter": filter,
			"last_id": last_id,
			"limit": limit,
		}
        url = "https://api-seller.ozon.ru/v3/product/info/stocks"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_product_import_prices(self, prices):
        """
        Обновить цену

        https://docs.ozon.ru/api/seller/#operation/ProductAPI_ImportProductsPrices
        """

        data = {
            "prices": prices,
        }
        url = "https://api-seller.ozon.ru/v1/product/import/prices"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v4_product_info_prices(self, filter={}, last_id=None, limit=100):
        """
        Получить информацию о цене товара

        https://docs.ozon.ru/api/seller/#operation/ProductAPI_GetProductInfoPricesV4
        """

        data = {
			"filter": filter,
			"last_id": last_id,
			"limit": limit,
		}
        url = "https://api-seller.ozon.ru/v4/product/info/prices"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_product_info_discounted(self, discounted_skus):
        """
        Узнать информацию об уценке и основном товаре по SKU уценённого товара

        https://docs.ozon.ru/api/seller/#operation/ProductAPI_GetProductInfoDiscounted
        """

        data = {
            "discounted_skus": discounted_skus,
        }
        url = "https://api-seller.ozon.ru/v1/product/info/discounted"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_product_update_discount(self, discount, product_id):
        """
        Установить скидку на уценённый товар

        https://docs.ozon.ru/api/seller/#operation/ProductAPI_ProductUpdateDiscount
        """

        data = {
            "discount": discount,
            "product_id": product_id,
        }
        url = "https://api-seller.ozon.ru/v1/product/update/discount"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
