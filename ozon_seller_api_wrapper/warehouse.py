import requests


class WarehouseAPI:
    """
    Склады

    https://docs.ozon.ru/api/seller/#tag/WarehouseAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v1_warehouse_list(self):
        """
        Список складов

        https://docs.ozon.ru/api/seller/#operation/WarehouseAPI_WarehouseList
        """
        
        data = {}
        url = "https://api-seller.ozon.ru/v1/warehouse/list"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_delivery_method_list(self, filter={}, last_id=None, limit=100):
        """
        Список методов доставки склада

        https://docs.ozon.ru/api/seller/#operation/WarehouseAPI_DeliveryMethodList
        """

        data = {
			"filter": filter,
			"last_id": last_id,
			"limit": limit,
		}
        url = "https://api-seller.ozon.ru/v1/delivery-method/list"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
