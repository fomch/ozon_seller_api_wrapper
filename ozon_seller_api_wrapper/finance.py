import requests


class FinanceAPI:
    """
    Финансовые отчёты

    https://docs.ozon.ru/api/seller/#tag/FinanceAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }
    
    def v2_finance_realization(self, month, year):
        """
        Отчёт о реализации товаров (версия 2)

        https://api-seller.ozon.ru/v2/finance/realization
        """

        data = {
            "month": month,
            "year": year,
        }
        url = "https://api-seller.ozon.ru/v2/finance/realization"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v3_finance_transaction_list(self, filter={}, last_id=None, limit=100):
        """
        Список транзакций

        https://docs.ozon.ru/api/seller/#operation/FinanceAPI_FinanceTransactionListV3
        """

        data = {
			"filter": filter,
			"last_id": last_id,
			"limit": limit,
		}
        url = "https://api-seller.ozon.ru/v3/finance/transaction/list"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v3_finance_transaction_totals(self, date, posting_number, transaction_type):
        """
        Суммы транзакций

        https://docs.ozon.ru/api/seller/#operation/FinanceAPI_FinanceTransactionTotalV3
        """

        data = {
            "date": date,
            "posting_number": posting_number,
            "transaction_type": transaction_type,
        }
        url = "https://api-seller.ozon.ru/v3/finance/transaction/totals"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
