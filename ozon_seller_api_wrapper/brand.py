import requests


class BrandAPI:
    """
    Сертификаты брендов

    https://docs.ozon.ru/api/seller/#tag/BrandAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v1_brand_company_certification_list(self, page, page_size):
        """
        Список сертифицируемых брендов

        https://api-seller.ozon.ru/v1/brand/company-certification/list
        """

        data = {
            "page": page,
            "page_size": page_size,
        }
        url = "https://api-seller.ozon.ru/v1/brand/company-certification/list"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
