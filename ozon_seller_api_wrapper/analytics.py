import requests


class AnalyticsAPI:
    """
    Аналитические отчёты

    https://docs.ozon.ru/api/seller/#tag/AnalyticsAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }
    
    def v1_analytics_data(self, date_from, date_to, metrics, dimension, 
                          filters, sort, limit, offset):
        """
        Данные аналитики

        https://docs.ozon.ru/api/seller/#operation/AnalyticsAPI_AnalyticsGetData
        """

        data = {
            "date_from": date_from,
            "date_to": date_to,
            "metrics": metrics,
            "dimension": dimension,
            "filters": filters,
            "sort": sort,
            "limit": limit,
            "offset": offset,
        }
        url = "https://api-seller.ozon.ru/v1/analytics/data"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v2_analytics_stock_on_warehouses(self, limit, offset, warehouse_type):
        """
        Отчёт по остаткам и товарам

        https://docs.ozon.ru/api/seller/#operation/AnalyticsAPI_AnalyticsGetStockOnWarehousesV2
        """

        data = {
            "limit": limit,
            "offset": offset,
            "warehouse_type": warehouse_type,
        }
        url = "https://api-seller.ozon.ru/v2/analytics/stock_on_warehouses"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
