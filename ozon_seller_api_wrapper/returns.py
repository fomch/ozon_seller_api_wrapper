import requests


class ReturnsAPI:
    """
    Возвраты товаров FBO и FBS

    https://docs.ozon.ru/api/seller/#tag/ReturnsAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v3_returns_company_fbo(self, filter={}, last_id=None, limit=100):
        """
        Получить информацию о возвратах FBO

        https://docs.ozon.ru/api/seller/#operation/ReturnsAPI_GetReturnsCompanyFbo
        """

        data = {
			"filter": filter,
			"last_id": last_id,
			"limit": limit,
		}
        url = "https://api-seller.ozon.ru/v3/returns/company/fbo"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v3_returns_company_fbs(self, filter={}, last_id=None, limit=100):
        """
        Получить информацию о возвратах FBS

        https://docs.ozon.ru/api/seller/#operation/ReturnsAPI_GetReturnsCompanyFBSv3
        """

        data = {
			"filter": filter,
			"last_id": last_id,
			"limit": limit,
		}
        url = "https://api-seller.ozon.ru/v3/returns/company/fbs"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
