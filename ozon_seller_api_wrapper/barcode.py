import requests


class BarcodeAPI:
    """
    Штрихкоды товаров

    https://docs.ozon.ru/api/seller/#tag/BarcodeAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v1_barcode_add(self, barcodes):
        """
        Привязать штрихкод к товару

        https://api-seller.ozon.ru/v1/barcode/add
        """

        data = {
            "barcodes": barcodes,
        }
        url = "https://api-seller.ozon.ru/v1/barcode/add"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_barcode_generate(self, product_ids):
        """
        Создать штрихкод для товара

        https://docs.ozon.ru/api/seller/#operation/generate-barcode
        """

        data = {
            "product_ids": product_ids,
        }
        url = "https://api-seller.ozon.ru/v1/barcode/generate"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
