import requests


class CategoryAPI:
    """
    Атрибуты и характеристики Ozon

    https://docs.ozon.ru/api/seller/#tag/CategoryAPI
    """

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {
            "Client-Id": client_id,
            "Api-Key": client_secret,
            "Content-Type": "application/json",
        }

    def v1_description_category_tree(self, language="DEFAULT"):
        """
        Дерево категорий и типов товаров

        https://docs.ozon.ru/api/seller/#operation/DescriptionCategoryAPI_GetTree
        """

        data = {
            "language": language,
        }
        url = "https://api-seller.ozon.ru/v1/description-category/tree"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def v1_description_category_attribute(self, description_category_id, language, type_id):
        """
        Список характеристик категории

        https://docs.ozon.ru/api/seller/#operation/DescriptionCategoryAPI_GetAttributes
        """

        data = {
            "description_category_id": description_category_id,
            "language": language,
            "type_id": type_id,
        }
        url = "https://api-seller.ozon.ru/v1/description-category/attribute"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
    
    def v1_description_category_attribute_values(self, 
                                                 attribute_id, 
                                                 description_category_id,
                                                 language,
                                                 last_value_id,
                                                 limit,
                                                 type_id
                                                 ):
        """
        Справочник значений характеристики

        https://docs.ozon.ru/api/seller/#operation/DescriptionCategoryAPI_GetAttributeValues
        """

        data = {
            "attribute_id": attribute_id,
            "description_category_id": description_category_id,
            "language": language,
            "last_value_id": last_value_id,
            "limit": limit,
            "type_id": type_id,
        }
        url = "https://api-seller.ozon.ru/v1/description-category/attribute/values"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
